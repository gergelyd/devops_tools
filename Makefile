CURRENT_DIR := $(shell pwd)
 
.PHONY: clean
 
help:
	@echo "Run make with:"
	@echo " > up             ...to start vagrant"
	@echo " > ssh            ...to destroy vagrant"
	@echo " > halt           ...to stop vagrant"
	@echo " > destroy        ...to destroy vagrant"



up:
	vagrant box add -f packer/nodebox nodebox.box
	vagrant up --provision

ssh:
	vagrant ssh

halt:
	vagrant halt
 

destroy:
	vagrant destroy
	vagrant box remove packer/nodebox