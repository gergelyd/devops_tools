# Workshop 19/12/2018

---------------------------------
1230-1300 **BigData course**

- Overview of the semantics of the Architecture
- BigData big picture
- What is Datascience
    - [Visual intro to Machine Learning](http://www.r2d3.us/visual-intro-to-machine-learning-part-1/)
        

---------------------------------
1300-1330 **Python Course**

- Python Environments and Setup
    - Virtualbox? Ansible? Docker? What the heck?
    - which IDE to use
        - Jupyter
        - PyCharm Community edition
        - IntelliJ
    - Jupyter Labs is used for these labs
        - easy setup
        - could be used for Spark later
        - widely used for exploratory analysis
        - easy to find docker images for any subjects

- Python in high level
    - what it is for
    - main use cases
    - [Learn Python](learn-python/README.md):
        - Getting Started Python
        - Operators
        - Data Types
        - Control Flow


---------------------------------
1400-1500 **Python Course**

[Go to Machine-Learning-with-Python folder](Machine-Learning-with-Python/README.md)
- Introducing main packages for Datascience for exploratory
    - matplotlib
    - pandas
- Data manipulation in Python with Pandas:
    - Basics of Pandas Dataframe
    - PandasPractice


--------------------------------- 
1500-1530 **Machine Learning Course**

- Machine Learning basics:
    - [Linear Regression Practice](myML/iris_classification.ipynb)
    - [Classification](myML/MachineLearningLinearRegression.ipynb)
    
---------------------------------     
1530-1600 **Machine Learning Course**

- Classification: Logistic Regression Classification
- Web Scraping with BeautifulSoup



#### [References]
 - https://github.com/ZuzooVn/machine-learning-for-software-engineers
 - https://media.readthedocs.org/pdf/pandasguide/latest/pandasguide.pdf
 
# Workshop ??/03/2019

---------------------------------
**BigData course**

- How to scale up ML with Spark
- Kafka streaming
- Docker, making Microservices with Kubernetes

---------------------------------
**Python Course**

- Python Environments and Setup
- Python in gist


---------------------------------
**Machine Learning Course**

