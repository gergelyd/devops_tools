# Dev tools

## Prerequirement

You need to set up the environment by installing the products:
 - vagrant
 - Virtualbox
 - git
 
After installing them, you need to clone the project from Bitbucket.

    $ mkdir $HOME/repos
    $ cd $HOME/repos
    $ git clone https://gergelyd@bitbucket.org/gergelyd/devops_tools.git
 
_Note: if you are on Windows, then your user folder does not contain any special characters._
 
##### Installing vagrant

Follow the documentation on https://www.vagrantup.com/docs/installation/

Download page for all OS: https://www.vagrantup.com/downloads.html

On Centos 64bit:

    cd ~/Downloads
    wget https://releases.hashicorp.com/vagrant/2.2.2/vagrant_2.2.2_x86_64.rpm
    rpm -i vagrant_2.2.2_x86_64.rpm
    

##### Installing Virtualbox

Download page: https://www.virtualbox.org/wiki/Downloads

Optional: Also load and install the Virtualbox 5.2.22 Oracle VM VirtualBox Extension Pack.
https://download.virtualbox.org/virtualbox/5.2.22/Oracle_VM_VirtualBox_Extension_Pack-5.2.22.vbox-extpack


##### Installing Git

Installation guide: https://git-scm.com/book/en/v2/Getting-Started-Installing-Git

_Note: if you are on Windows box, choose the `Checkout as-is, commit Unix-style` option when it ask for the line ending methods during the installation._

## Starting up image with vagrant

As you have already cloned the devops_tools repository, go there and first make it up, then you can ssh into the box.

    $ cd $HOME/repos
    $ make up
    ...
    $ make ssh
    ...
    vagrant$ <you are in the nodebox vmbox>

## Included tools in the box

 - Docker images:
    - Jupyter notebook with Spark
    - Wordpress
 

You can check what docker images are installed on the box. 
First check the images with `docker images` command:

    REPOSITORY                        TAG                 IMAGE ID            CREATED             SIZE
    jupyter/sparkmagic                latest              9de78ec5f2e3        17 minutes ago      962 MB
    jupyter/sparkmagic-livy           latest              4c4f57ba4d55        21 minutes ago      1.69 GB
    docker.io/wordpress               latest              55b751a7663f        3 days ago          423 MB
    docker.io/mysql                   5.7                 ae6b78bedf88        3 weeks ago         372 MB
    docker.io/jupyter/base-notebook   ae885c0a6226        d49ef225184f        15 months ago       604 MB
    docker.io/gettyimages/spark       2.1.0-hadoop-2.7    5b4ddc368fa0        22 months ago       746 MB


Optional check:
You can start up the Jupyter and Spark images by running:
    
    $ docker run -p 8888:8888 -v /vagrant/examples:/home/jovyan -d --name jupyter jupyter/sparkmagic 
    
    $ docker run -p 8998:8998 -v /vagrant/examples:/home/jovyan -d --name spark jupyter/sparkmagic-livy
    
    $ docker run -p 8888:8888 -v /vagrant/examples:/home/jovyan -d --name scipy jupyter/scipy-notebook:2c80cf3537ca

Getting token for Jupyter:
    
    $ docker logs scipy | grep token=
    
Once it is up, you can check in the browser if the Jupyter has started on the 8888 port:

    http://localhost:8888
        or alternatively
    http://1.2.3.4:8888    

Enjoy the workshop :) !

#### License

Copyright (c) 2018 Ketsh Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.